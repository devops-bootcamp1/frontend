FROM node:8 as builder
ARG YARN_ENV=development

WORKDIR /usr/app
COPY package*.json ./
RUN yarn
COPY src/ ./src/
COPY public/ ./public/
RUN yarn build:${YARN_ENV}

FROM nginx:1.15.5
COPY --from=builder /usr/app/build/ /usr/share/nginx/html
EXPOSE 80